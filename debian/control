Source: autopkgtest
Maintainer: Debian CI team <team+ci@tracker.debian.org>
Uploaders: Ian Jackson <ijackson@chiark.greenend.org.uk>,
           Martin Pitt <mpitt@debian.org>,
           Antonio Terceiro <terceiro@debian.org>,
           Paul Gevers <elbrus@debian.org>
Section: devel
Priority: optional
Standards-Version: 4.4.0
Build-Depends: debhelper (>= 9),
               procps,
               pycodestyle | pep8,
               pyflakes3 | pyflakes,
               python3 (>= 3.1),
               python3-debian,
               python3-docutils,
               python3-mock
Vcs-Git: https://salsa.debian.org/ci-team/autopkgtest.git
Vcs-Browser: https://salsa.debian.org/ci-team/autopkgtest

Package: autopkgtest
Architecture: all
Depends: apt-utils,
         libdpkg-perl,
         procps,
         python3,
         python3-debian,
         ${misc:Depends}
Recommends: autodep8
Suggests: lxc,
          lxd,
          ovmf,
          qemu-efi-aarch64,
          qemu-efi-arm,
          qemu-system,
          qemu-utils,
          schroot,
          vmdb2
Breaks: debci (<< 1.7~)
Description: automatic as-installed testing for Debian packages
 autopkgtest runs tests on binary packages.  The tests are run on the
 package as installed on a testbed system (which may be found via a
 virtualisation or containment system).  The tests are expected to be
 supplied in the corresponding Debian source package.
 .
 See autopkgtest(1) and /usr/share/doc/autopkgtest.
 Depending on which virtualization server you want to use, you need to
 install additional packages (schroot, lxc, lxd, or qemu-system)
 .
 For generating tests of well-known source packages such as Perl and Ruby
 libraries you should install the autodep8 package.
